from django.shortcuts import redirect, render
from django.core.paginator import Paginator, EmptyPage
from rdflib import Graph
from SPARQLWrapper import SPARQLWrapper, JSON

# Create your views here.
def main(request):
    return render(request, "main.html", {})

# Get Uni abstract, type, and country BY University Name from dbpedia
def get_uni_detail_dbpedia(univ):
    a = univ.title().split()
    for i in range(len(a)):
        if (a[i]=='Of') or (a[i]=='And') or (a[i]=='For') or (a[i]=='On') or (a[i]=='In')   or (a[i]=='De') :
            a[i] = a[i].lower()
    uni_name = ' '.join(a)

    result = {}
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery(''' 
    SELECT DISTINCT ?univName ?typeLabel ?abstract ?countryName
    WHERE {{
        ?univ rdf:type dbo:University ;
        dbp:name "{}"@en ;
        dbp:name ?univName .
        OPTIONAL {{ ?univ dbo:abstract ?abstract }} .
        OPTIONAL {{ ?univ dbo:type ?type .
                    ?type rdfs:label ?typeLabel .
                    FILTER (lang(?typeLabel) = 'en') }} .
        OPTIONAL {{ ?univ dbp:country ?country .
                    ?country foaf:name ?countryName }} .
        FILTER (lang(?abstract) = 'en') 
    }}
    LIMIT 1

    '''.format(uni_name))
    sparql.setReturnFormat(JSON)
    res = sparql.query().convert()
    res = res['results']['bindings'][0]
    for i in res:
        result[i] = res[i]['value']
    return result

def get_uni_list_by_country(country):
    a = country.title().split()
    for i in range(len(a)):
        if (a[i]=='Of') or (a[i]=='And') or (a[i]=='For') or (a[i]=='On') or (a[i]=='In')   or (a[i]=='De') :
            a[i] = a[i].lower()
    country_name = ' '.join(a)

    result = []
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery(''' 
    SELECT DISTINCT ?uniName
    WHERE {{
        ?univ rdf:type dbo:University ;
        dbo:country ?country .

        ?country foaf:name "{}"@en .

        ?univ dbp:name ?uniName .

        FILTER (lang(?uniName) = 'en') .
    }}

    '''.format(country_name))
    sparql.setReturnFormat(JSON)
    res = sparql.query().convert()
    res = res['results']['bindings']
    for i in range(len(res)):
        uni_name = res[i]['uniName']['value']
        result.append(uni_name)
    return result

def get_uni_list_by_city(city):
    a = city.title().split()
    for i in range(len(a)):
        if (a[i]=='Of') or (a[i]=='And') or (a[i]=='For') or (a[i]=='On') or (a[i]=='In')   or (a[i]=='De') :
            a[i] = a[i].lower()
    city_name = ' '.join(a)

    result = []
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery(''' 
    SELECT ?uniName ?cityName
    WHERE {{
        ?univ rdf:type dbo:University ;
                dbp:city ?city .

        {{ ?city dbp:name "{}"@en ;
            dbp:name ?cityName }}
        UNION 
        {{ ?city dbp:officialName "{}"@en  ;
            dbp:officialName ?cityName }} .
        
        OPTIONAL {{ ?univ dbp:name ?uniName }} .

        FILTER (lang(?uniName) = 'en') .
    }}
    '''.format(city_name, city_name))
    sparql.setReturnFormat(JSON)
    res = sparql.query().convert()
    res = res['results']['bindings']
    for i in range(len(res)):
        uni_name = res[i]['uniName']['value']
        result.append(uni_name)
    return result

# Get Uni WorldRank, NationalRank, EmploymentRanki BY University Name from local DB
def get_all_rank_by_uni_name(univ):
    a = univ.title().split()
    for i in range(len(a)):
        if (a[i]=='Of') or (a[i]=='And') or (a[i]=='For') or (a[i]=='On') or (a[i]=='In')   or (a[i]=='De') :
            a[i] = a[i].lower()
    uni_name = ' '.join(a)

    result = {}
    sparql = SPARQLWrapper("http://localhost:3030/unirankings/query")
    sparql.setQuery(''' 
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX dbo: <http://dbpedia.org/ontology/>
    PREFIX ex: <http://example.org/>

    SELECT DISTINCT ?uniName ?wr2012 ?wr2013 ?wr2014 ?wr2015 ?ae2012 ?ae2013 ?ae2014 ?ae2015 ?nr2012 ?nr2013 ?nr2014 ?nr2015
    WHERE {{
    ?uniName rdf:type dbo:University;
        ex:name "{}".
    OPTIONAL {{?uniName ex:2012_alumni_employment_rank ?ae2012 }}.
    OPTIONAL {{?uniName ex:2013_alumni_employment_rank ?ae2013 }}.
    OPTIONAL {{?uniName ex:2014_alumni_employment_rank ?ae2014 }}.
    OPTIONAL {{?uniName ex:2015_alumni_employment_rank ?ae2015 }}.
    
    OPTIONAL {{?uniName ex:2012_world_rank ?wr2012 }} .
    OPTIONAL {{?uniName ex:2013_world_rank ?wr2013 }} .
    OPTIONAL {{?uniName ex:2014_world_rank ?wr2014 }} .
    OPTIONAL {{?uniName ex:2015_world_rank ?wr2015 }} .

    OPTIONAL {{?uniName ex:2012_national_rank ?nr2012 }} .
    OPTIONAL {{?uniName ex:2013_national_rank ?nr2013 }} .
    OPTIONAL {{?uniName ex:2014_national_rank ?nr2014 }} .
    OPTIONAL {{?uniName ex:2015_national_rank ?nr2015 }} .

  }}
    ORDER BY ASC (?uniName)
    '''.format(uni_name))
    sparql.setReturnFormat(JSON)
    res = sparql.query().convert()
    res = res['results']['bindings'][0]
    for i in res:
        result[i] = res[i]['value']
    return result

def search_university(request):
    if request.method == "POST":
        univ = request.POST.get("search")

        search_result = {}
        univ_list = []
        
        try:
            abstract = get_uni_detail_dbpedia(univ)['abstract']
            abstract = abstract.split('.')
            abstract = '.'.join(abstract[0:3]) + '.'

            url_detail = univ.replace(" ", "_")
            
            univ_detail = {}
            univ_detail['university'] = univ
            univ_detail['abstract'] = abstract
            univ_detail['url_detail'] = url_detail

            univ_list.append(univ_detail)
        except:
            pass

        search_result = {
            "univ_list" : univ_list
        }

        return render(request, "list_university.html", search_result)
    
    else:
        return render(request, "search_university.html")

def search_country(request):
    if request.method == "POST":
        country = request.POST.get("search")

        list_univ = get_uni_list_by_country(country)

        search_result = {}
        univ_list = []

        for univ in list_univ:
            try:
                if univ == "":
                    abstract = get_uni_detail_dbpedia(univ)['abstract']
                    abstract = abstract.split('.')
                    abstract = '.'.join(abstract[0:3]) + '.'

                    url_detail = univ.replace(" ", "_")
                    
                    univ_detail = {}
                    univ_detail['university'] = univ
                    univ_detail['abstract'] = abstract
                    univ_detail['url_detail'] = url_detail

                    univ_list.append(univ_detail)
            except:
                continue

        search_result = {
            "univ_list" : univ_list
        }

        return render(request, "list_country.html", search_result)
    else:
        return render(request, "search_country.html")
    
def search_city(request):
    if request.method == "POST":
        city = request.POST.get("search")

        list_univ = get_uni_list_by_city(city)
        search_result = {}
        univ_list = []

        for univ in list_univ:
            try:
                if univ == "":
                    abstract = get_uni_detail_dbpedia(univ)['abstract']
                    abstract = abstract.split('.')
                    abstract = '.'.join(abstract[0:3]) + '.'

                    url_detail = univ.replace(" ", "_")
                    
                    univ_detail = {}
                    univ_detail['university'] = univ
                    univ_detail['abstract'] = abstract
                    univ_detail['url_detail'] = url_detail

                    univ_list.append(univ_detail)
            except:
                continue

        search_result = {
            "univ_list" : univ_list
        }

        return render(request, "list_city.html", search_result)
    else:
        return render(request, "search_city.html")

def detail(request, univ):
    univ_name = univ.replace("_", " ")

    dbpedia = get_uni_detail_dbpedia(univ_name)

    try:
        local = get_all_rank_by_uni_name(univ_name)
        for i in local:
            dbpedia[i] = local[i]
    except:
        local = {'wr2012': '-', 'wr2013': '-', 'wr2014': '-', 'wr2015': '-', 'ae2012': '-', 'ae2013': '-', 'ae2014': '-', 'ae2015': '-', 'nr2012': '-', 'nr2013': '-', 'nr2014': '-', 'nr2015': '-'}
    
    for i in local:
        dbpedia[i] = local[i]

    return render(request, "detail_university.html", dbpedia)
        