from django.urls import path
from . import views

urlpatterns = [
    path('', views.main, name='main'),
    path('search/university', views.search_university, name='search_university'),
    path('search/country', views.search_country, name='search_country'),
    path('search/city', views.search_city, name='search_city'),
    path('detail/<str:univ>', views.detail, name='detail'),
]
